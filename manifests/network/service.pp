class system::network::service {
 case $operatingsystem {
    redhat, centos: {
      service { 'network':
      ensure => 'running',
      enable => true,
      }
    }
   debian, ubuntu: {
      service { 'networking':
      ensure => 'running',
      enable => true,
      }

   }
    default: { fail("Unrecognized operating system for network configuration") }
    }
}
